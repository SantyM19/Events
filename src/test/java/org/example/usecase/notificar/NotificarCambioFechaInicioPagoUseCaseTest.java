package org.example.usecase.notificar;

import co.com.sofka.business.generic.ServiceBuilder;
import co.com.sofka.business.generic.UseCaseHandler;
import co.com.sofka.business.support.TriggeredEvent;
import org.example.eventos.eventos.events.InicioPagoModificado;
import org.example.eventos.eventos.value.CronogramaId;
import org.example.eventos.eventos.value.InicioPagos;
import org.example.eventos.generics.value.Cuenta;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class NotificarCambioFechaInicioPagoUseCaseTest {
    private NotificarCambioFechaInicioPagoUseCase notificarCambioFechaInicioPagoUseCase;

    private NotificarCambioFechaInicioPagos notificarCambioFechaPagosService;
    private SenderEmailService senderEmailService;

    @BeforeEach
    public void setup() {
        notificarCambioFechaInicioPagoUseCase = new NotificarCambioFechaInicioPagoUseCase();
        notificarCambioFechaPagosService = mock(NotificarCambioFechaInicioPagos.class);
        senderEmailService = mock(SenderEmailService.class);

        ServiceBuilder builder = new ServiceBuilder();
        builder.addService(notificarCambioFechaPagosService);
        builder.addService(senderEmailService);

        notificarCambioFechaInicioPagoUseCase.addServiceBuilder(builder);
    }

    @Test
    void agregarComentarioNotificarHappyPath() {
        var event = new InicioPagoModificado(
                CronogramaId.of("xx-xx"),
                new InicioPagos("2022")
        );

        when(notificarCambioFechaPagosService.getCorreoPorIdUsuario(any())).thenReturn(new Cuenta("santyM19@gmail.com"));
        doNothing().when(senderEmailService).sendEmail(any(), anyString());

        var response = UseCaseHandler.getInstance()
                .syncExecutor(
                        notificarCambioFechaInicioPagoUseCase,
                        new TriggeredEvent<>(event)
                );

        verify(notificarCambioFechaPagosService).getCorreoPorIdUsuario(any());
        verify(senderEmailService).sendEmail(any(), anyString());
    }



}