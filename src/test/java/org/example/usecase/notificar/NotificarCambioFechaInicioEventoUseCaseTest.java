package org.example.usecase.notificar;

import co.com.sofka.business.generic.ServiceBuilder;
import co.com.sofka.business.generic.UseCaseHandler;
import co.com.sofka.business.support.TriggeredEvent;
import org.example.eventos.eventos.events.InicioEventoModificado;
import org.example.eventos.eventos.events.InicioPagoModificado;
import org.example.eventos.eventos.value.CronogramaId;
import org.example.eventos.eventos.value.InicioEvento;
import org.example.eventos.eventos.value.InicioPagos;
import org.example.eventos.generics.value.Cuenta;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;

class NotificarCambioFechaInicioEventoUseCaseTest {
    private NotificarCambioFechaInicioEventoUseCase notificarCambioFechaInicioEventoUseCase;

    private NotificarCambioFechaInicioEvento notificarCambioFechaEventosService;
    private SenderEmailService senderEmailService;

    @BeforeEach
    public void setup() {
        notificarCambioFechaInicioEventoUseCase = new NotificarCambioFechaInicioEventoUseCase();
        notificarCambioFechaEventosService = mock(NotificarCambioFechaInicioEvento.class);
        senderEmailService = mock(SenderEmailService.class);

        ServiceBuilder builder = new ServiceBuilder();
        builder.addService(notificarCambioFechaEventosService);
        builder.addService(senderEmailService);

        notificarCambioFechaInicioEventoUseCase.addServiceBuilder(builder);
    }

    @Test
    void agregarComentarioNotificarHappyPath() {
        var event = new InicioEventoModificado(
                CronogramaId.of("xx-xx"),
                new InicioEvento("2022")
        );

        when(notificarCambioFechaEventosService.getCorreoPorIdUsuario(any())).thenReturn(new Cuenta("santyM19@gmail.com"));
        doNothing().when(senderEmailService).sendEmail(any(), anyString());

        var response = UseCaseHandler.getInstance()
                .syncExecutor(
                        notificarCambioFechaInicioEventoUseCase,
                        new TriggeredEvent<>(event)
                );

        verify(notificarCambioFechaEventosService).getCorreoPorIdUsuario(any());
        verify(senderEmailService).sendEmail(any(), anyString());
    }


}