package org.example.usecase.eventos;

import co.com.sofka.business.generic.UseCaseHandler;
import co.com.sofka.business.support.RequestCommand;
import com.sun.nio.sctp.IllegalReceiveException;
import org.example.eventos.eventos.Cronograma;
import org.example.eventos.eventos.Descripcion;
import org.example.eventos.eventos.Reglamento;
import org.example.eventos.eventos.commands.CrearEvento;
import org.example.eventos.eventos.events.EventoCreado;
import org.example.eventos.eventos.value.*;
import org.example.eventos.generics.Usuario;
import org.example.eventos.generics.value.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

class CrearEventoUseCaseTest {
    private CrearEventoUseCase crearEventoUseCase;

    @BeforeEach
    public void setup(){
        crearEventoUseCase = new CrearEventoUseCase();
    }

    @Test
    void crearRetoHappyPath(){
        //arrange
        var command = new CrearEvento(
                EventosId.of("xxx-xxxx"),
                new Reglamento(ReglamentosId.of("xxx"),
                        new Acceso("Mayores de 18 años"),
                        new HashSet<>()),
                new Descripcion(DescripcionId.of("ddd"),
                        new Nombre("Campeonato Lol"),
                        new Trasfondo("Lograras ganar esta Regional, trae tu equipo")),
                new Cronograma(CronogramaId.of("ccc"),
                        new InicioPagos("2022" ),
                        new InicioEvento("2023"),
                        new FinEvento("2024") ),
                new Usuario(UsuarioId.of("uuu"),
                        new Nombre("Me :v"),
                        new Cuenta("@nose"),
                        new Nickname("tuPape"),
                        new Edad(25))
        );

        //act
        var response = UseCaseHandler.getInstance().syncExecutor(
                crearEventoUseCase, new RequestCommand<>(command)
        ).orElseThrow();

        var events = response.getDomainEvents();


        //asserts
        EventoCreado eventoCreado = (EventoCreado)events.get(0);

        Assertions.assertEquals(new Nombre("Campeonato Lol"), eventoCreado.getDescripcion().nombreEvento());
        Assertions.assertEquals(new Trasfondo("Lograras ganar esta Regional, trae tu equipo"), eventoCreado.getDescripcion().trasfondo());

        Assertions.assertEquals(new Acceso("Mayores de 18 años"), eventoCreado.getReglas().acceso());

        Assertions.assertEquals(new InicioPagos("2022" ),eventoCreado.getCronograma().inicioPagos());
        Assertions.assertEquals(new InicioEvento("2023"),eventoCreado.getCronograma().inicioEvento());
        Assertions.assertEquals(new FinEvento("2024"),eventoCreado.getCronograma().finEvento());

        Assertions.assertEquals(new Nombre("Me :v"),eventoCreado.getPostor().nombre());
        Assertions.assertEquals(new Cuenta("@nose"),eventoCreado.getPostor().cuenta());
        Assertions.assertEquals(new Nickname("tuPape"),eventoCreado.getPostor().nickname());
        Assertions.assertEquals( new Edad(25),eventoCreado.getPostor().edad());

    }

}