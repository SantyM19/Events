package org.example.usecase.eventos;

import co.com.sofka.business.generic.UseCaseHandler;
import co.com.sofka.business.repository.DomainEventRepository;
import co.com.sofka.business.support.RequestCommand;
import co.com.sofka.domain.generic.DomainEvent;
import org.example.eventos.eventos.Cronograma;
import org.example.eventos.eventos.Descripcion;
import org.example.eventos.eventos.Reglamento;
import org.example.eventos.eventos.commands.ModificarDescripcion;
import org.example.eventos.eventos.events.DescripcionModificada;
import org.example.eventos.eventos.events.EventoCreado;
import org.example.eventos.eventos.value.*;
import org.example.eventos.generics.Usuario;
import org.example.eventos.generics.value.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ModificarDescripcionUseCaseTest {
    private ModificarDescripcionUseCase modificarDescripcionUseCase;

    private DomainEventRepository repository;

    @BeforeEach
    public void setup(){
        modificarDescripcionUseCase = new ModificarDescripcionUseCase();
        repository = mock(DomainEventRepository.class);
        modificarDescripcionUseCase.addRepository(repository);
    }

    @Test
    void modificarDescripcionHappyPath() {
        //arrange
        var command = new ModificarDescripcion(
                EventosId.of("eee"),
                DescripcionId.of("rrr"),
                new Nombre("Campeonato Italiano"),
                new Trasfondo("Primera Liga De Lol")
        );

        when(repository.getEventsBy(any())).thenReturn(events());

        //act
        var response = UseCaseHandler.getInstance()
                .setIdentifyExecutor("aaaaa")
                .syncExecutor(
                        modificarDescripcionUseCase, new RequestCommand<>(command))
                .orElseThrow();

        System.out.println(response);

        var events = response.getDomainEvents();


        //asserts
        DescripcionModificada eventoCreado = (DescripcionModificada) events.get(0);
        Assertions.assertEquals(new Nombre("Campeonato Italiano"), eventoCreado.nombreEvento());
        Assertions.assertEquals(new Trasfondo("Primera Liga De Lol"), eventoCreado.trasfondo());
    }

    private List<DomainEvent> events() {
        return List.of(new EventoCreado(
                new Reglamento(ReglamentosId.of("xxx"),
                        new Acceso("Mayores de 18 años"),
                        new HashSet<>()),
                new Descripcion(DescripcionId.of("ddd"),
                        new Nombre("Campeonato Lol"),
                        new Trasfondo("Lograras ganar esta Regional, trae tu equipo")),
                new Cronograma(CronogramaId.of("ccc"),
                        new InicioPagos("2022" ),
                        new InicioEvento("2023"),
                        new FinEvento("2024") ),
                new Usuario(UsuarioId.of("uuu"),
                        new Nombre("Me :v"),
                        new Cuenta("@nose"),
                        new Nickname("tuPape"),
                        new Edad(25))
        ));
    }
}