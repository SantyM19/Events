package org.example.usecase.administrador;

import co.com.sofka.business.generic.UseCaseHandler;
import co.com.sofka.business.support.RequestCommand;
import org.example.eventos.administrador.GestionRecaudos;
import org.example.eventos.administrador.MedioVirtual;
import org.example.eventos.administrador.commands.CrearAdministrador;
import org.example.eventos.administrador.event.AdministradorCreado;
import org.example.eventos.administrador.value.*;
import org.example.eventos.eventos.value.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashSet;

class CrearAdministradorUseCaseTest {
    private CrearAdministradorUseCase crearAdministradorUseCase;

    @BeforeEach
    public void setup(){
        crearAdministradorUseCase = new CrearAdministradorUseCase();
    }

    @Test
    void crearRetoHappyPath(){
        //arrange
        var command = new CrearAdministrador(
                AdministradorId.of("xxx-xxxx"),
                new MedioVirtual(MedioVirtualId.of("mmm"),
                        new Plataforma("GoogleMeet"),
                        new Regla("Solo Correos Inscritos")),
                new HashSet<>(),
                new GestionRecaudos(GestionRecaudosId.of("ggg"),
                        new Precio(56_000.0),
                        new HashSet<>(),
                        new Pago(true)),
                new EventosId()
                );

        //act
        var response = UseCaseHandler.getInstance().syncExecutor(
                crearAdministradorUseCase, new RequestCommand<>(command)
        ).orElseThrow();

        var events = response.getDomainEvents();


        //asserts
        AdministradorCreado eventoCreado = (AdministradorCreado) events.get(0);

        Assertions.assertEquals(new Plataforma("GoogleMeet"), eventoCreado.getMedioVirtual().plataforma());
        Assertions.assertEquals(new Regla("Solo Correos Inscritos"), eventoCreado.getMedioVirtual().regla());

        Assertions.assertEquals(new Pago(true), eventoCreado.getRecaudos().pago());
        Assertions.assertEquals(new Precio(56000.0), eventoCreado.getRecaudos().precio());

    }

}