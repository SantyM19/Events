package org.example.usecase.administrador;

import co.com.sofka.business.generic.UseCaseHandler;
import co.com.sofka.business.repository.DomainEventRepository;
import co.com.sofka.business.support.RequestCommand;
import co.com.sofka.domain.generic.DomainEvent;
import org.example.eventos.administrador.GestionRecaudos;
import org.example.eventos.administrador.MedioVirtual;
import org.example.eventos.administrador.commands.ModificarPrecio;
import org.example.eventos.administrador.event.AdministradorCreado;
import org.example.eventos.administrador.event.PrecioModificado;
import org.example.eventos.administrador.value.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ModificarPrecioUseCaseTest {
    private ModificarPrecioUseCase modificarPrecioUseCase;

    private DomainEventRepository repository;

    @BeforeEach
    public void setup(){
        modificarPrecioUseCase = new ModificarPrecioUseCase();
        repository = mock(DomainEventRepository.class);
        modificarPrecioUseCase.addRepository(repository);
    }

    @Test
    void modificarPagoHappyPath() {
        //arrange
        var command = new ModificarPrecio(
                AdministradorId.of("aaa"),
                GestionRecaudosId.of("ggg"),
                new Precio(60_000.0)
        );

        when(repository.getEventsBy(any())).thenReturn(events());

        //act
        var response = UseCaseHandler.getInstance()
                .setIdentifyExecutor("aaaaa")
                .syncExecutor(
                        modificarPrecioUseCase, new RequestCommand<>(command))
                .orElseThrow();

        System.out.println(response);

        var events = response.getDomainEvents();


        //asserts
        PrecioModificado eventoCreado = (PrecioModificado) events.get(0);
        Assertions.assertEquals(new Precio(60_000.0), eventoCreado.precio());
    }

    private List<DomainEvent> events() {
        return List.of(new AdministradorCreado(
                new MedioVirtual(MedioVirtualId.of("mmm"),
                        new Plataforma("GoogleMeet"),
                        new Regla("Solo Correos Inscritos")),
                new HashSet<>(),
                new GestionRecaudos(GestionRecaudosId.of("ggg"),
                        new Precio(56_000.0),
                        new HashSet<>(),
                        new Pago(false))
        ));
    }

}