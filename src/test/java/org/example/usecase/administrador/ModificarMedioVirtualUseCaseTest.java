package org.example.usecase.administrador;

import co.com.sofka.business.generic.UseCaseHandler;
import co.com.sofka.business.repository.DomainEventRepository;
import co.com.sofka.business.support.RequestCommand;
import co.com.sofka.domain.generic.DomainEvent;
import org.example.eventos.administrador.GestionRecaudos;
import org.example.eventos.administrador.MedioVirtual;
import org.example.eventos.administrador.commands.ModificarMedioVirtual;
import org.example.eventos.administrador.commands.ModificarPago;
import org.example.eventos.administrador.event.AdministradorCreado;
import org.example.eventos.administrador.event.MedioVirtualModificado;
import org.example.eventos.administrador.event.PagoModificado;
import org.example.eventos.administrador.value.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ModificarMedioVirtualUseCaseTest {
    private ModificarMedioVirtualUseCase modificarMedioVirtualUseCase;

    private DomainEventRepository repository;

    @BeforeEach
    public void setup(){
        modificarMedioVirtualUseCase = new ModificarMedioVirtualUseCase();
        repository = mock(DomainEventRepository.class);
        modificarMedioVirtualUseCase.addRepository(repository);
    }

    @Test
    void modificarMedioVirtualHappyPath() {
        //arrange
        var command = new ModificarMedioVirtual(
                AdministradorId.of("aaa"),
                MedioVirtualId.of("mmm"),
                new Plataforma("Zoom"),
                new Regla("Todos con enlace")
        );

        when(repository.getEventsBy(any())).thenReturn(events());

        //act
        var response = UseCaseHandler.getInstance()
                .setIdentifyExecutor("aaaaa")
                .syncExecutor(
                        modificarMedioVirtualUseCase, new RequestCommand<>(command))
                .orElseThrow();

        System.out.println(response);

        var events = response.getDomainEvents();


        //asserts
        MedioVirtualModificado eventoCreado = (MedioVirtualModificado) events.get(0);
        Assertions.assertEquals(new Plataforma("Zoom"), eventoCreado.plataforma());
        Assertions.assertEquals(new Regla("Todos con enlace"), eventoCreado.regla());
    }

    private List<DomainEvent> events() {
        return List.of(new AdministradorCreado(
                new MedioVirtual(MedioVirtualId.of("mmm"),
                        new Plataforma("GoogleMeet"),
                        new Regla("Solo Correos Inscritos")),
                new HashSet<>(),
                new GestionRecaudos(GestionRecaudosId.of("ggg"),
                        new Precio(56_000.0),
                        new HashSet<>(),
                        new Pago(false))
        ));
    }

}