package org.example.eventos.generics.value;

import co.com.sofka.domain.generic.ValueObject;
import com.sun.nio.sctp.IllegalReceiveException;

import java.util.Objects;

public class Cuenta implements ValueObject<String> {
    private final String value;

    public Cuenta(String value){
        Objects.requireNonNull(value,  "El nickname es obligatorio");
        if(value.isBlank()){
            throw new IllegalReceiveException("El nickname no puede estar vacio");
        }
        if(value.length() < 4){
            throw new IllegalReceiveException("El nickname debe de tener mas de 4 caracteres");
        }
        if(value.length() > 200){
            throw new IllegalReceiveException("El nickname debe de tener menos de 200 caracteres");
        }
        if(value.matches("^[Aa-z\\s]+$")){
            throw new IllegalReceiveException("El nickname no puede tener caracteres especiales ");
        }
        this.value = value;

    }

    @Override
    public String value() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cuenta cuenta = (Cuenta) o;
        return Objects.equals(value, cuenta.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
