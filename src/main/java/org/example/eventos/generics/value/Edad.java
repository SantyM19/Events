package org.example.eventos.generics.value;

import co.com.sofka.domain.generic.ValueObject;

import java.util.Objects;

public class Edad implements ValueObject<Integer> {
    private final Integer value;

    public Edad(Integer value) {
        Objects.requireNonNull(value);
        if(value < 1 ){
            throw new IllegalArgumentException("El precio no puede ser igual o menor a cero");
        }
        this.value= value;
    }

    @Override
    public Integer value() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Edad edad = (Edad) o;
        return Objects.equals(value, edad.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
