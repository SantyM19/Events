package org.example.eventos.generics;

import co.com.sofka.domain.generic.Entity;
import org.example.eventos.generics.value.*;

import java.util.Objects;

public class Usuario extends Entity<UsuarioId> {
    private Nombre nombre;
    private Cuenta cuenta;
    private Nickname nickname;
    private Edad edad;

    public Usuario(UsuarioId entityId, Nombre nombre, Cuenta cuenta, Nickname nickname, Edad edad) {
        super(entityId);
        this.nombre = nombre;
        this.cuenta = cuenta;
        this.nickname = nickname;
        this.edad = edad;
    }

    public Nombre nombre() {
        return nombre;
    }

    public Cuenta cuenta() {
        return cuenta;
    }

    public Nickname nickname() {
        return nickname;
    }

    public Edad edad() {
        return edad;
    }

    public void modificarNombre(Nombre nombre){
        this.nombre = Objects.requireNonNull(nombre);
    }
    public void modificarEdad(Edad edad){
        this.edad = Objects.requireNonNull(edad);
    }
    public void modificarCuenta(Cuenta cuenta){
        this.cuenta = Objects.requireNonNull(cuenta);
    }
    public void modificarNickName(Nickname nickname){
        this.nickname = Objects.requireNonNull(nickname);
    }

    public void modificarse(Nombre nombre, Cuenta cuenta, Nickname nickname, Edad edad) {
        this.nombre = nombre;
        this.cuenta = cuenta;
        this.nickname = nickname;
        this.edad = edad;
    }
}
