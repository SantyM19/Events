package org.example.eventos.administrador;

import co.com.sofka.domain.generic.Entity;
import org.example.eventos.administrador.value.Plataforma;
import org.example.eventos.administrador.value.Regla;
import org.example.eventos.administrador.value.MedioVirtualId;

import java.util.Objects;

public class MedioVirtual extends Entity<MedioVirtualId> {
    private Plataforma plataforma;
    private Regla regla;

    public MedioVirtual(MedioVirtualId entityId, Plataforma plataforma, Regla regla) {
        super(entityId);
        this.plataforma = plataforma;
        this.regla = regla;
    }

    public Plataforma plataforma() {
        return plataforma;
    }

    public Regla regla() {
        return regla;
    }

    public void modificarPlataforma(Plataforma plataforma){
        this.plataforma = Objects.requireNonNull(plataforma);
    }
    public void modificarRegla(Regla regla){
        this.regla = Objects.requireNonNull(regla);
    }

    public void modificarse(Plataforma plataforma, Regla regla) {
        this.plataforma = plataforma;
        this.regla = regla;
    }
}
