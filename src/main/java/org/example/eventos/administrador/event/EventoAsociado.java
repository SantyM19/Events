package org.example.eventos.administrador.event;

import co.com.sofka.domain.generic.DomainEvent;
import org.example.eventos.eventos.value.EventosId;

public class EventoAsociado extends DomainEvent {
    private EventosId eventoId;

    public EventoAsociado(EventosId eventoId) {
        super("example.administrador.eventoasociado");
        this.eventoId = eventoId;
    }

    public EventosId getEventoId() {
        return eventoId;
    }
}
