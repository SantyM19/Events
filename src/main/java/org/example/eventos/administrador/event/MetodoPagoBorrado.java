package org.example.eventos.administrador.event;

import co.com.sofka.domain.generic.DomainEvent;
import org.example.eventos.administrador.value.GestionRecaudosId;
import org.example.eventos.administrador.value.MetodoPago;

public class MetodoPagoBorrado extends DomainEvent {
    private GestionRecaudosId entityId;
    private MetodoPago metodopago;

    public MetodoPagoBorrado(GestionRecaudosId entityId, MetodoPago pago) {
        super("example.administrador.metodopagoborrado");
        this.entityId = entityId;
        this.metodopago = pago;
    }

    public GestionRecaudosId entityId() {
        return entityId;
    }

    public MetodoPago metodopago() {
        return metodopago;
    }
}
