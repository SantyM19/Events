package org.example.eventos.administrador.event;

import co.com.sofka.domain.generic.DomainEvent;
import org.example.eventos.administrador.value.GestionRecaudosId;
import org.example.eventos.administrador.value.MetodoPago;

public class MetodoPagoModificado extends DomainEvent {
    private GestionRecaudosId entityId;
    private MetodoPago metodopago;
    private MetodoPago newMetodoPago;

    public MetodoPagoModificado(GestionRecaudosId entityId, MetodoPago metodopago, MetodoPago newMetodoPago) {
        super("example.administrador.metodopagomodificado");
        this.entityId = entityId;
        this.metodopago = metodopago;
        this.newMetodoPago = newMetodoPago;
    }

    public GestionRecaudosId entityId() {
        return entityId;
    }

    public MetodoPago metodopago() {
        return metodopago;
    }

    public MetodoPago newMetodoPago() {
        return newMetodoPago;
    }
}
