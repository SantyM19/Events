package org.example.eventos.administrador.event;

import co.com.sofka.domain.generic.DomainEvent;
import org.example.eventos.administrador.GestionRecaudos;
import org.example.eventos.administrador.MedioVirtual;
import org.example.eventos.generics.Usuario;

import java.util.Set;

public class AdministradorCreado extends DomainEvent {
    private final MedioVirtual medioVirtual;
    private final Set<Usuario> participantes;
    private final GestionRecaudos recaudos;

    public AdministradorCreado(MedioVirtual medioVirtual, Set<Usuario> participantes, GestionRecaudos recaudos) {
        super("example.administrador.administradorcreado");
        this.medioVirtual = medioVirtual;
        this.participantes = participantes;
        this.recaudos = recaudos;
    }

    public MedioVirtual getMedioVirtual() {
        return medioVirtual;
    }

    public Set<Usuario> getParticipantes() {
        return participantes;
    }

    public GestionRecaudos getRecaudos() {
        return recaudos;
    }
}
