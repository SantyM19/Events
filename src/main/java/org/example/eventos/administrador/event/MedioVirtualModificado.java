package org.example.eventos.administrador.event;

import co.com.sofka.domain.generic.DomainEvent;
import org.example.eventos.administrador.value.MedioVirtualId;
import org.example.eventos.administrador.value.Plataforma;
import org.example.eventos.administrador.value.Regla;

public class MedioVirtualModificado extends DomainEvent {
    private MedioVirtualId entityId;
    private Plataforma plataforma;
    private Regla regla;

    public MedioVirtualModificado(MedioVirtualId entityId, Plataforma plataforma, Regla regla) {
        super("example.administrador.mediovirtualmodificado");
        this.entityId = entityId;
        this.plataforma = plataforma;
        this.regla = regla;
    }

    public MedioVirtualId entityId() {
        return entityId;
    }

    public Plataforma plataforma() {
        return plataforma;
    }

    public Regla regla() {
        return regla;
    }
}
