package org.example.eventos.administrador.event;

import co.com.sofka.domain.generic.DomainEvent;
import org.example.eventos.administrador.value.GestionRecaudosId;
import org.example.eventos.administrador.value.Precio;

public class PrecioModificado extends DomainEvent {
    private GestionRecaudosId entityId;
    private Precio precio;

    public PrecioModificado(GestionRecaudosId entityId, Precio precio) {
        super("example.administrador.preciomodificado");
        this.entityId = entityId;
        this.precio = precio;
    }

    public GestionRecaudosId entityId() {
        return entityId;
    }

    public Precio precio() {
        return precio;
    }
}
