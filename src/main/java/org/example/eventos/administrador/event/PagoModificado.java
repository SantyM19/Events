package org.example.eventos.administrador.event;

import co.com.sofka.domain.generic.DomainEvent;
import org.example.eventos.administrador.value.GestionRecaudosId;
import org.example.eventos.administrador.value.Pago;

public class PagoModificado extends DomainEvent {
    private GestionRecaudosId entityId;
    private Pago pago;

    public PagoModificado(GestionRecaudosId entityId, Pago pago) {
        super("example.administrador.pagomodificado");
        this.entityId = entityId;
        this.pago = pago;
    }

    public GestionRecaudosId entityId() {
        return entityId;
    }

    public Pago pago() {
        return pago;
    }
}
