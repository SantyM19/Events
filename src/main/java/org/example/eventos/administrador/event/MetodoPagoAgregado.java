package org.example.eventos.administrador.event;

import co.com.sofka.domain.generic.DomainEvent;
import org.example.eventos.administrador.value.GestionRecaudosId;
import org.example.eventos.administrador.value.MetodoPago;

public class MetodoPagoAgregado extends DomainEvent {
    private GestionRecaudosId entityId;
    private MetodoPago metodopago;

    public MetodoPagoAgregado(GestionRecaudosId entityId, MetodoPago metodopago) {
        super("example.administrador.metodopagoagregado");
        this.entityId = entityId;
        this.metodopago = metodopago;
    }

    public GestionRecaudosId entityId() {
        return entityId;
    }

    public MetodoPago metodopago() {
        return metodopago;
    }
}
