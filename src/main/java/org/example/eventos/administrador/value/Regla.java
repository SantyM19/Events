package org.example.eventos.administrador.value;

import co.com.sofka.domain.generic.ValueObject;
import com.sun.nio.sctp.IllegalReceiveException;

import java.util.Objects;

public class Regla implements ValueObject<String> {
    private final String value;

    public Regla(String value){
        Objects.requireNonNull(value,  "La Regla es obligatoria");
        if(value.isBlank()){
            throw new IllegalReceiveException("La Regla no puede estar vacia");
        }
        if(value.length() < 4){
            throw new IllegalReceiveException("La Regla debe de tener mas de 4 caracteres");
        }
        if(value.length() > 200){
            throw new IllegalReceiveException("La Regla debe de tener menos de 200 caracteres");
        }
        if(value.matches("^[Aa-z\\s]+$")){
            throw new IllegalReceiveException("La Regla no puede tener caracteres especiales ");
        }
        this.value = value;

    }

    @Override
    public String value() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Regla regla = (Regla) o;
        return Objects.equals(value, regla.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
