package org.example.eventos.administrador.value;

import co.com.sofka.domain.generic.Identity;

public class MedioVirtualId extends Identity {
    public MedioVirtualId(){

    }
    private MedioVirtualId(String id) {
        super(id);
    }

    public static MedioVirtualId of(String id) {
        return new MedioVirtualId(id);
    }
}
