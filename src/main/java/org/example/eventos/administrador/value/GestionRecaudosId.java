package org.example.eventos.administrador.value;

import co.com.sofka.domain.generic.Identity;
public class GestionRecaudosId extends Identity{
    public GestionRecaudosId(){

    }
    private GestionRecaudosId(String id) {
        super(id);
    }

    public static GestionRecaudosId of(String id) {
        return new GestionRecaudosId(id);
    }
}
