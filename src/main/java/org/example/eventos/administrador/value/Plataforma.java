package org.example.eventos.administrador.value;

import co.com.sofka.domain.generic.ValueObject;
import com.sun.nio.sctp.IllegalReceiveException;

import java.util.Objects;

public class Plataforma implements ValueObject<String> {
    private final String value;

    public Plataforma(String value){
        Objects.requireNonNull(value,  "El nombre de la plataforma es obligatorio");
        if(value.isBlank()){
            throw new IllegalReceiveException("El nombre de la plataforma no puede estar vacio");
        }
        if(value.length() < 4){
            throw new IllegalReceiveException("El nombre de la plataforma debe de tener mas de 4 caracteres");
        }
        if(value.length() > 200){
            throw new IllegalReceiveException("El nombre de la plataforma debe de tener menos de 200 caracteres");
        }
        if(value.matches("^[Aa-z\\s]+$")){
            throw new IllegalReceiveException("El nombre de la plataforma no puede tener caracteres especiales ");
        }
        this.value = value;

    }

    @Override
    public String value() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Plataforma that = (Plataforma) o;
        return Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
