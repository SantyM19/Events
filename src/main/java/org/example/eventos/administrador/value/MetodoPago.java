package org.example.eventos.administrador.value;

import co.com.sofka.domain.generic.ValueObject;
import com.sun.nio.sctp.IllegalReceiveException;

import java.util.Objects;

public class MetodoPago implements ValueObject<String> {
    private final String value;

    public MetodoPago(String value){
        Objects.requireNonNull(value,  "El Metodo de Pago es obligatorio");
        if(value.isBlank()){
            throw new IllegalReceiveException("El Metodo de Pago no puede estar vacio");
        }
        if(value.length() < 4){
            throw new IllegalReceiveException("El Metodo de Pago debe de tener mas de 4 caracteres");
        }
        if(value.length() > 200){
            throw new IllegalReceiveException("El Metodo de Pago debe de tener menos de 200 caracteres");
        }
        if(value.matches("^[Aa-z\\s]+$")){
            throw new IllegalReceiveException("El Metodo de Pago no puede tener caracteres especiales ");
        }
        this.value = value;

    }

    @Override
    public String value() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetodoPago that = (MetodoPago) o;
        return Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
