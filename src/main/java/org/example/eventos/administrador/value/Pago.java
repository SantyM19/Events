package org.example.eventos.administrador.value;

import co.com.sofka.domain.generic.ValueObject;

import java.util.Objects;

public class Pago implements ValueObject<Boolean> {
    private final Boolean value;

    public Pago(Boolean value) {
        Objects.requireNonNull(value);
        this.value= value;
    }

    @Override
    public Boolean value() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pago pago = (Pago) o;
        return Objects.equals(value, pago.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
