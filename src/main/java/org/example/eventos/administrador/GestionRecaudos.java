package org.example.eventos.administrador;

import co.com.sofka.domain.generic.Entity;
import org.example.eventos.administrador.value.*;

import java.util.Objects;
import java.util.Set;

public class GestionRecaudos extends Entity<GestionRecaudosId> {
    private Precio precio;
    private final Set<MetodoPago> metodoPagos;
    private Pago pago;

    public GestionRecaudos(GestionRecaudosId entityId, Precio precio, Set<MetodoPago> metodoPagos, Pago pago) {
        super(entityId);
        this.precio = precio;
        this.metodoPagos = metodoPagos;
        this.pago = pago;
    }

    public Precio precio() {
        return precio;
    }

    public Set<MetodoPago> metodoPagos() {
        return metodoPagos;
    }

    public Pago pago() {
        return pago;
    }

    public void modificarPrecio(Precio precio){
        this.precio = Objects.requireNonNull(precio);
    }

    public void modificarPago(Pago pago){
        this.pago = Objects.requireNonNull(pago);
    }

    public void agregarMetodoPago(MetodoPago metodoPago){
        this.metodoPagos.add(Objects.requireNonNull(metodoPago));
    }

    public void borrarMetodoPago(MetodoPago metodoPago){
        this.metodoPagos.remove(Objects.requireNonNull(metodoPago));
    }

    public void modificarMetodoPago(MetodoPago metodoPago, MetodoPago newMetodoPago){
        this.metodoPagos.remove(Objects.requireNonNull(metodoPago));
        this.metodoPagos.add(Objects.requireNonNull(newMetodoPago));
    }

}
