package org.example.eventos.administrador.commands;

import co.com.sofka.domain.generic.Command;
import org.example.eventos.administrador.value.AdministradorId;
import org.example.eventos.generics.value.Cuenta;
import org.example.eventos.generics.value.Edad;
import org.example.eventos.generics.value.Nickname;
import org.example.eventos.generics.value.UsuarioId;
import org.example.eventos.generics.value.Nombre;

public class AgregarUnParticipante implements Command {
    private final AdministradorId administradorId;
    private final UsuarioId entityId;
    private final Nombre nombre;
    private final Cuenta cuenta;
    private final Nickname nickname;
    private final Edad edad;

    public AgregarUnParticipante(AdministradorId administradorId, UsuarioId entityId, Nombre nombre, Cuenta cuenta, Nickname nickname, Edad edad) {
        this.administradorId = administradorId;
        this.entityId = entityId;
        this.nombre = nombre;
        this.cuenta = cuenta;
        this.nickname = nickname;
        this.edad = edad;
    }

    public AdministradorId getAdministradorId() {
        return administradorId;
    }

    public UsuarioId getEntityId() {
        return entityId;
    }

    public Nombre getNombre() {
        return nombre;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public Nickname getNickname() {
        return nickname;
    }

    public Edad getEdad() {
        return edad;
    }
}
