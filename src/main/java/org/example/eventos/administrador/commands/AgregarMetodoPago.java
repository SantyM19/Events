package org.example.eventos.administrador.commands;

import co.com.sofka.domain.generic.Command;
import org.example.eventos.administrador.value.AdministradorId;
import org.example.eventos.administrador.value.GestionRecaudosId;
import org.example.eventos.administrador.value.MetodoPago;

public class AgregarMetodoPago implements Command {
    private final AdministradorId administradorId;
    private final GestionRecaudosId entityId;
    private final MetodoPago pago;

    public AgregarMetodoPago(AdministradorId administradorId, GestionRecaudosId entityId, MetodoPago pago) {
        this.administradorId = administradorId;
        this.entityId = entityId;
        this.pago = pago;
    }

    public GestionRecaudosId getEntityId() {
        return entityId;
    }

    public MetodoPago getPago() {
        return pago;
    }

    public AdministradorId getAdministradorId() {
        return administradorId;
    }
}
