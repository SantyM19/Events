package org.example.eventos.administrador.commands;

import co.com.sofka.domain.generic.Command;
import org.example.eventos.administrador.GestionRecaudos;
import org.example.eventos.administrador.MedioVirtual;
import org.example.eventos.administrador.value.AdministradorId;
import org.example.eventos.eventos.value.EventosId;
import org.example.eventos.generics.Usuario;

import java.util.Set;

public class CrearAdministrador implements Command {
    private final AdministradorId administradorId;
    private final MedioVirtual medioVirtual;
    private final Set<Usuario> participantes;
    private final GestionRecaudos recaudos;
    private final EventosId eventoId;

    public CrearAdministrador(AdministradorId administradorId, MedioVirtual medioVirtual, Set<Usuario> participantes, GestionRecaudos recaudos, EventosId eventoId) {
        this.administradorId = administradorId;
        this.medioVirtual = medioVirtual;
        this.participantes = participantes;
        this.recaudos = recaudos;
        this.eventoId = eventoId;
    }

    public AdministradorId getAdministradorId() {
        return administradorId;
    }

    public MedioVirtual getMedioVirtual() {
        return medioVirtual;
    }

    public Set<Usuario> getParticipantes() {
        return participantes;
    }

    public GestionRecaudos getRecaudos() {
        return recaudos;
    }

    public EventosId getEventoId() {
        return eventoId;
    }
}
