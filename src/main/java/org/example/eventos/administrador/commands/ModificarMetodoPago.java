package org.example.eventos.administrador.commands;

import co.com.sofka.domain.generic.Command;
import org.example.eventos.administrador.value.AdministradorId;
import org.example.eventos.administrador.value.GestionRecaudosId;
import org.example.eventos.administrador.value.MetodoPago;

public class ModificarMetodoPago implements Command {
    private final AdministradorId administradorId;
    private final GestionRecaudosId entityId;
    private final MetodoPago pago;
    private final MetodoPago newMetodoPago;

    public ModificarMetodoPago(AdministradorId administradorId, GestionRecaudosId entityId, MetodoPago pago, MetodoPago newMetodoPago) {
        this.administradorId = administradorId;
        this.entityId = entityId;
        this.pago = pago;
        this.newMetodoPago = newMetodoPago;
    }

    public AdministradorId getAdministradorId() {
        return administradorId;
    }

    public GestionRecaudosId getEntityId() {
        return entityId;
    }

    public MetodoPago getPago() {
        return pago;
    }

    public MetodoPago getNewMetodoPago() {
        return newMetodoPago;
    }
}
