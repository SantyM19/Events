package org.example.eventos.administrador.commands;

import co.com.sofka.domain.generic.Command;
import org.example.eventos.administrador.value.AdministradorId;
import org.example.eventos.administrador.value.GestionRecaudosId;
import org.example.eventos.administrador.value.MetodoPago;

public class BorrarMetodoPago implements Command {
    private final GestionRecaudosId entityId;
    private final MetodoPago pago;
    private final AdministradorId administradorId;

    public BorrarMetodoPago(GestionRecaudosId entityId, MetodoPago pago, AdministradorId administradorId) {
        this.entityId = entityId;
        this.pago = pago;
        this.administradorId = administradorId;
    }

    public AdministradorId getAdministradorId() {
        return administradorId;
    }

    public GestionRecaudosId getEntityId() {
        return entityId;
    }

    public MetodoPago getPago() {
        return pago;
    }
}
