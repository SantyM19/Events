package org.example.eventos.administrador.commands;

import co.com.sofka.domain.generic.Command;
import org.example.eventos.administrador.value.AdministradorId;
import org.example.eventos.administrador.value.GestionRecaudosId;
import org.example.eventos.administrador.value.Pago;

public class ModificarPago implements Command {
    private final AdministradorId administradorId;
    private final GestionRecaudosId entityId;
    private final Pago pago;

    public ModificarPago(AdministradorId administradorId, GestionRecaudosId entityId, Pago pago) {
        this.administradorId = administradorId;
        this.entityId = entityId;
        this.pago = pago;
    }

    public AdministradorId getAdministradorId() {
        return administradorId;
    }

    public GestionRecaudosId getEntityId() {
        return entityId;
    }

    public Pago getPago() {
        return pago;
    }
}
