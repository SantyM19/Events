package org.example.eventos.administrador.commands;

import co.com.sofka.domain.generic.Command;
import org.example.eventos.administrador.value.AdministradorId;
import org.example.eventos.administrador.value.MedioVirtualId;
import org.example.eventos.administrador.value.Plataforma;
import org.example.eventos.administrador.value.Regla;

public class ModificarMedioVirtual implements Command {
    private final AdministradorId administradorId;
    private final MedioVirtualId entityId;
    private final Plataforma plataforma;
    private final Regla regla;

    public ModificarMedioVirtual(AdministradorId administradorId, MedioVirtualId entityId, Plataforma plataforma, Regla regla) {
        this.administradorId = administradorId;
        this.entityId = entityId;
        this.plataforma = plataforma;
        this.regla = regla;
    }

    public AdministradorId getAdministradorId() {
        return administradorId;
    }

    public MedioVirtualId getEntityId() {
        return entityId;
    }

    public Plataforma getPlataforma() {
        return plataforma;
    }

    public Regla getRegla() {
        return regla;
    }
}
