package org.example.eventos.administrador.commands;

import co.com.sofka.domain.generic.Command;
import org.example.eventos.administrador.value.AdministradorId;
import org.example.eventos.administrador.value.GestionRecaudosId;
import org.example.eventos.administrador.value.Precio;

public class ModificarPrecio implements Command {
    private final AdministradorId administradorId;
    private final GestionRecaudosId entityId;
    private final Precio precio;

    public ModificarPrecio(AdministradorId administradorId1, GestionRecaudosId entityId, Precio precio) {
        this.administradorId = administradorId1;
        this.entityId = entityId;
        this.precio = precio;
    }

    public GestionRecaudosId getEntityId() {
        return entityId;
    }

    public Precio getPrecio() {
        return precio;
    }

    public AdministradorId getAdministradorId() {
        return administradorId;
    }
}
