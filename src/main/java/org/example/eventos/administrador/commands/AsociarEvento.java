package org.example.eventos.administrador.commands;

import co.com.sofka.domain.generic.Command;
import org.example.eventos.administrador.value.AdministradorId;
import org.example.eventos.eventos.value.EventosId;

public class AsociarEvento implements Command {
    private final AdministradorId administradorId;
    private final EventosId eventoId;

    public AsociarEvento(AdministradorId administradorId, EventosId eventoId) {
        this.administradorId = administradorId;
        this.eventoId = eventoId;
    }

    public AdministradorId getAdministradorId() {
        return administradorId;
    }

    public EventosId getEventoId() {
        return eventoId;
    }
}
