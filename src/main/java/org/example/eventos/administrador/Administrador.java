package org.example.eventos.administrador;

import co.com.sofka.domain.generic.AggregateEvent;
import co.com.sofka.domain.generic.DomainEvent;
import org.example.eventos.administrador.event.*;
import org.example.eventos.administrador.value.*;
import org.example.eventos.eventos.value.EventosId;
import org.example.eventos.generics.value.*;
import org.example.eventos.generics.Usuario;

import java.util.List;
import java.util.Objects;
import java.util.Set;

public class Administrador extends AggregateEvent<AdministradorId> {

    protected MedioVirtual medioVirtual;
    protected Set<Usuario> participantes;
    protected GestionRecaudos recaudos;
    protected EventosId eventoId;

    public Administrador(AdministradorId entityId, MedioVirtual medioVirtual, Set<Usuario> participantes, GestionRecaudos recaudos) {
        super(entityId);
        appendChange(new AdministradorCreado(medioVirtual, participantes, recaudos)).apply();
    }

    private Administrador (AdministradorId entityId){
        super(entityId);
        subscribe(new AdministradorChange(this));
    }

    public static Administrador from(AdministradorId id, List<DomainEvent> retrieveEvents) {
        var administrador = new Administrador(id);
        retrieveEvents.forEach(administrador::applyEvent);
        return administrador;
    }

    public void asociarEvento(EventosId eventoId){
        Objects.requireNonNull(eventoId);
        appendChange(new EventoAsociado(eventoId)).apply();
    }

    public void modificarPrecio(GestionRecaudosId entityId, Precio precio){
        Objects.requireNonNull(entityId);
        Objects.requireNonNull(precio);
        appendChange(new PrecioModificado(entityId, precio)).apply();
    }

    public void modificarPago(GestionRecaudosId entityId, Pago pago){
        Objects.requireNonNull(entityId);
        Objects.requireNonNull(pago);
        appendChange(new PagoModificado(entityId, pago)).apply();
    }

    public void modificarMetodoPago(GestionRecaudosId entityId, MetodoPago pago, MetodoPago newMetodoPago){
        Objects.requireNonNull(entityId);
        Objects.requireNonNull(pago);
        Objects.requireNonNull(newMetodoPago);
        appendChange(new MetodoPagoModificado(entityId, pago, newMetodoPago)).apply();
    }

    public void agregarMetodoPago(GestionRecaudosId entityId, MetodoPago pago){
        Objects.requireNonNull(entityId);
        Objects.requireNonNull(pago);
        appendChange(new MetodoPagoAgregado(entityId, pago)).apply();
    }

    public void borrarMetodoPago(GestionRecaudosId entityId, MetodoPago pago){
        Objects.requireNonNull(entityId);
        Objects.requireNonNull(pago);
        appendChange(new MetodoPagoBorrado(entityId, pago)).apply();
    }

    public void modificarMedioVirtual(MedioVirtualId entityId, Plataforma plataforma, Regla regla){
        Objects.requireNonNull(entityId);
        Objects.requireNonNull(plataforma);
        Objects.requireNonNull(regla);
        appendChange(new MedioVirtualModificado(entityId, plataforma,regla)).apply();
    }

    public void agregarUnParticipante(UsuarioId entityId, Nombre nombre, Cuenta cuenta, Nickname nickname, Edad edad){
        Objects.requireNonNull(entityId);
        Objects.requireNonNull(nombre);
        Objects.requireNonNull(cuenta);
        Objects.requireNonNull(nickname);
        Objects.requireNonNull(edad);
        appendChange(new ParticipanteAgregado(entityId, nombre, cuenta, nickname, edad)).apply();
    }

    public void ModificarParticipante(UsuarioId entityId, Nombre nombre, Cuenta cuenta, Nickname nickname, Edad edad){
        Objects.requireNonNull(entityId);
        Objects.requireNonNull(nombre);
        Objects.requireNonNull(cuenta);
        Objects.requireNonNull(nickname);
        Objects.requireNonNull(edad);
        appendChange(new ParticipanteModificado(entityId, nombre, cuenta, nickname, edad)).apply();
    }

    public MedioVirtual Organizador() {
        return medioVirtual;
    }

    public Set<Usuario> Participantes() {
        return participantes;
    }

    public GestionRecaudos Recaudos() {
        return recaudos;
    }
}
