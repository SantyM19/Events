package org.example.eventos.administrador;

import co.com.sofka.domain.generic.EventChange;
import org.example.eventos.administrador.event.*;

public class AdministradorChange extends EventChange {
    public AdministradorChange(Administrador administrador) {
        apply((AdministradorCreado event)->{
            administrador.recaudos = event.getRecaudos();
            administrador.participantes = event.getParticipantes();
            administrador.medioVirtual = event.getMedioVirtual();
        });

        apply((EventoAsociado event)->{
            administrador.eventoId = event.getEventoId();
        });

        apply((MedioVirtualModificado event)->{
            administrador.medioVirtual.modificarse(event.plataforma(), event.regla());
        });

        apply((MetodoPagoModificado event)->{
            administrador.modificarMetodoPago(event.entityId(), event.metodopago(), event.newMetodoPago());
        });

        apply((MetodoPagoAgregado event)->{
            administrador.agregarMetodoPago(event.entityId(), event.metodopago());
        });

        apply((MetodoPagoBorrado event)->{
            administrador.borrarMetodoPago(event.entityId(), event.metodopago());
        });

        apply((PagoModificado event)->{
            administrador.recaudos.modificarPago(event.pago());
        });

        apply((ParticipanteAgregado event)->{
            administrador.agregarUnParticipante(event.entityId(), event.nombre(), event.cuenta(), event.nickname(), event.edad());
        });

        apply((ParticipanteModificado event)->{
            administrador.ModificarParticipante(event.entityId(), event.nombre(), event.cuenta(), event.nickname(), event.edad());
        });

        apply((PrecioModificado event)->{
            administrador.recaudos.modificarPrecio(event.precio());
        });

    }
}
