package org.example.eventos.eventos;

import co.com.sofka.domain.generic.AggregateEvent;
import co.com.sofka.domain.generic.DomainEvent;
import org.example.eventos.generics.value.Cuenta;
import org.example.eventos.generics.value.Edad;
import org.example.eventos.generics.value.Nickname;
import org.example.eventos.generics.value.UsuarioId;
import org.example.eventos.eventos.events.*;
import org.example.eventos.eventos.value.*;
import org.example.eventos.generics.value.Nombre;
import org.example.eventos.generics.Usuario;

import java.util.List;
import java.util.Objects;

public class Evento extends AggregateEvent<EventosId> {
    protected Reglamento reglas;
    protected Descripcion descripcion;
    protected Cronograma cronograma;
    protected Usuario postor;

    public Evento(EventosId entityId, Reglamento reglas, Descripcion descripcion, Cronograma cronograma, Usuario postor) {
        super(entityId);
        appendChange(new EventoCreado(reglas, descripcion, cronograma, postor)).apply();
    }

    private Evento (EventosId entityId){
        super(entityId);
        subscribe(new EventoChange(this));
    }

    public static Evento from(EventosId id, List<DomainEvent> retrieveEvents) {
        var evento = new Evento(id);
        retrieveEvents.forEach(evento::applyEvent);
        return evento;
    }

    public void modificarDescripcion(DescripcionId entityId, Nombre nombreEvento, Trasfondo trasfondo){
        Objects.requireNonNull(entityId);
        Objects.requireNonNull(trasfondo);
        Objects.requireNonNull(nombreEvento);
        appendChange(new DescripcionModificada(entityId, nombreEvento, trasfondo)).apply();
    }

    public void modificarFechaInicioPago(CronogramaId entityId, InicioPagos inicioPagos){
        Objects.requireNonNull(entityId);
        Objects.requireNonNull(inicioPagos);
        appendChange(new InicioPagoModificado(entityId, inicioPagos)).apply();
    }

    public void modificarFechaInicioEvento(CronogramaId entityId, InicioEvento inicioEvento){
        Objects.requireNonNull(entityId);
        Objects.requireNonNull(inicioEvento);
        appendChange(new InicioEventoModificado(entityId, inicioEvento)).apply();
    }

    public void modificarFechaFinEvento(CronogramaId entityId, FinEvento finEvento){
        Objects.requireNonNull(entityId);
        Objects.requireNonNull(finEvento);
        appendChange(new FinEventoModificado(entityId, finEvento)).apply();
    }

    public void modificarAcceso(ReglamentosId entityId, Acceso acceso){
        Objects.requireNonNull(entityId);
        Objects.requireNonNull(acceso);
        appendChange(new AccesoModificado(entityId, acceso)).apply();
    }

    public void agregarCanal(ReglamentosId entityId, CanalComunicacion canal){
        Objects.requireNonNull(entityId);
        Objects.requireNonNull(canal);
        appendChange(new CanalAgregado(entityId, canal)).apply();
    }

    public void borrarCanal(ReglamentosId entityId, CanalComunicacion canal){
        Objects.requireNonNull(entityId);
        Objects.requireNonNull(canal);
        appendChange(new CanalBorrado(entityId, canal)).apply();
    }

    public void modificarCanal(ReglamentosId entityId, CanalComunicacion canal , CanalComunicacion newCanal){
        Objects.requireNonNull(entityId);
        Objects.requireNonNull(canal);
        Objects.requireNonNull(newCanal);
        appendChange(new CanalModificado(entityId, canal, newCanal)).apply();
    }

    public void modificarPonente(UsuarioId entityId, Nombre nombre, Cuenta cuenta, Nickname nickname, Edad edad){
        Objects.requireNonNull(entityId);
        Objects.requireNonNull(nombre);
        Objects.requireNonNull(cuenta);
        Objects.requireNonNull(nickname);
        Objects.requireNonNull(edad);
        appendChange(new PonenteModificado(entityId, nombre, cuenta, nickname, edad)).apply();
    }

    public Reglamento reglas() {
        return reglas;
    }

    public Descripcion descripcion() {
        return descripcion;
    }

    public Cronograma fechas() {
        return cronograma;
    }
}
