package org.example.eventos.eventos;

import co.com.sofka.domain.generic.EventChange;
import org.example.eventos.eventos.events.*;

public class EventoChange extends EventChange {
    public EventoChange(Evento evento) {
        apply((EventoCreado event)->{
            evento.descripcion = event.getDescripcion();
            evento.cronograma = event.getCronograma();
            evento.postor = event.getPostor();
            evento.reglas = event.getReglas();
        });

        apply((AccesoModificado event)->{
            evento.reglas.modificarAcceso(event.acceso());
        });

        apply((CanalAgregado event)->{
            evento.reglas.agregarCanal(event.canal());
        });

        apply((CanalBorrado event)->{
            evento.reglas.borrarCanal(event.canal());
        });

        apply((CanalModificado event)->{
            evento.reglas.modificarCanal(event.canal(), event.newCanal());
        });

        apply((DescripcionModificada event)->{
            evento.descripcion.modificarse(event.nombreEvento(), event.trasfondo());
        });

        apply((FinEventoModificado event)->{
            evento.cronograma.modificarFinEvento(event.finEvento());
        });

        apply((InicioEventoModificado event)->{
            evento.cronograma.modificarInicioEvento(event.inicioEvento());
        });

        apply((InicioPagoModificado event)->{
            evento.cronograma.modificarInicioPagos(event.inicioPagos());
        });

        apply((PonenteModificado event) -> {
            evento.postor.modificarse(event.nombre(), event.cuenta(), event.nickname(), event.edad());
        });

    }
}
