package org.example.eventos.eventos.value;

import co.com.sofka.domain.generic.Identity;

public class DescripcionId extends Identity {
    public DescripcionId(){

    }
    private DescripcionId(String id) {
        super(id);
    }

    public static DescripcionId of(String id) {
        return new DescripcionId(id);
    }
}
