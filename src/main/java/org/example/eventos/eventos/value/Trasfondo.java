package org.example.eventos.eventos.value;

import co.com.sofka.domain.generic.ValueObject;
import com.sun.nio.sctp.IllegalReceiveException;

import java.util.Objects;

public class Trasfondo implements ValueObject<String> {
    private final String value;

    public Trasfondo(String value){
        Objects.requireNonNull(value,  "El trasfondo es obligatorio");
        if(value.isBlank()){
            throw new IllegalReceiveException("El trasfondo no puede estar vacio");
        }
        if(value.length() < 4){
            throw new IllegalReceiveException("El trasfondo debe de tener mas de 4 caracteres");
        }
        if(value.length() > 200){
            throw new IllegalReceiveException("El trasfondo debe de tener menos de 200 caracteres");
        }
        if(value.matches("^[Aa-z\\s]+$")){
            throw new IllegalReceiveException("El trasfondo no puede tener caracteres especiales ");
        }
        this.value = value;

    }

    @Override
    public String value() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Trasfondo trasfondo = (Trasfondo) o;
        return Objects.equals(value, trasfondo.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
