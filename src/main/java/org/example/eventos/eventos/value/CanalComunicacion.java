package org.example.eventos.eventos.value;

import co.com.sofka.domain.generic.ValueObject;
import com.sun.nio.sctp.IllegalReceiveException;

import java.util.Objects;

public class CanalComunicacion implements ValueObject<String> {
    private final String value;

    public CanalComunicacion(String value){
        Objects.requireNonNull(value,  "El Canal de Comunicacion es obligatorio");
        if(value.isBlank()){
            throw new IllegalReceiveException("El Canal de Comunicacion no puede estar vacio");
        }
        if(value.length() < 4){
            throw new IllegalReceiveException("El Canal de Comunicacion debe de tener mas de 4 caracteres");
        }
        if(value.length() > 200){
            throw new IllegalReceiveException("El Canal de Comunicacion debe de tener menos de 200 caracteres");
        }
        if(value.matches("^[Aa-z\\s]+$")){
            throw new IllegalReceiveException("El Canal de Comunicacion no puede tener caracteres especiales ");
        }
        this.value = value;

    }

    @Override
    public String value() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CanalComunicacion that = (CanalComunicacion) o;
        return Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
