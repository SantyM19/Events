package org.example.eventos.eventos.value;

import co.com.sofka.domain.generic.Identity;

public class ReglamentosId extends Identity {
    public ReglamentosId(){

    }
    private ReglamentosId(String id) {
        super(id);
    }

    public static ReglamentosId of(String id) {
        return new ReglamentosId(id);
    }
}
