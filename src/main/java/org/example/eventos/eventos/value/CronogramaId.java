package org.example.eventos.eventos.value;

import co.com.sofka.domain.generic.Identity;

public class CronogramaId extends Identity {
    public CronogramaId(){

    }
    private CronogramaId(String id) {
        super(id);
    }

    public static CronogramaId of(String id) {
        return new CronogramaId(id);
    }
}
