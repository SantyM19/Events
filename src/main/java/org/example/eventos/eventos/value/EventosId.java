package org.example.eventos.eventos.value;

import co.com.sofka.domain.generic.Identity;

public class EventosId extends Identity {
    public EventosId(){

    }
    private EventosId(String id) {
        super(id);
    }

    public static EventosId of(String id) {
        return new EventosId(id);
    }
}
