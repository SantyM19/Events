package org.example.eventos.eventos;

import co.com.sofka.domain.generic.Entity;
import org.example.eventos.eventos.value.*;

import java.util.Objects;
import java.util.Set;

public class Reglamento extends Entity<ReglamentosId> {
    private Acceso acceso;
    private final Set<CanalComunicacion> canales;

    public Reglamento(ReglamentosId entityId, Acceso acceso, Set<CanalComunicacion> canales) {
        super(entityId);
        this.acceso = acceso;
        this.canales = canales;
    }

    public Acceso acceso() {
        return acceso;
    }

    public Set<CanalComunicacion> canales() {
        return canales;
    }

    public void modificarAcceso(Acceso acceso){
        this.acceso = Objects.requireNonNull(acceso);
    }

    public void agregarCanal(CanalComunicacion canal){
        this.canales.add(Objects.requireNonNull(canal));
    }

    public void borrarCanal(CanalComunicacion canal){
        this.canales.remove(Objects.requireNonNull(canal));
    }

    public void modificarCanal(CanalComunicacion canal, CanalComunicacion newCanal){
        this.canales.remove(Objects.requireNonNull(canal));
        this.canales.add(Objects.requireNonNull(newCanal));
    }
}
