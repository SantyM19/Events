package org.example.eventos.eventos;

import co.com.sofka.domain.generic.Entity;
import org.example.eventos.generics.value.Nombre;
import org.example.eventos.eventos.value.DescripcionId;
import org.example.eventos.eventos.value.Trasfondo;

import java.util.Objects;

public class Descripcion extends Entity<DescripcionId> {
    private Nombre nombreEvento;
    private Trasfondo trasfondo;

    public Descripcion(DescripcionId entityId, Nombre nombreEvento, Trasfondo trasfondo) {
        super(entityId);
        this.nombreEvento = nombreEvento;
        this.trasfondo = trasfondo;
    }

    public Nombre nombreEvento() {
        return nombreEvento;
    }

    public Trasfondo trasfondo() {
        return trasfondo;
    }

    public void modificarNombreEvento(Nombre nombreEvento){
        this.nombreEvento = Objects.requireNonNull(nombreEvento);
    }
    public void modificarTrasfondo(Trasfondo trasfondo){
        this.trasfondo = Objects.requireNonNull(trasfondo);
    }

    public void modificarse( Nombre nombreEvento, Trasfondo trasfondo) {
        this.nombreEvento = Objects.requireNonNull(nombreEvento);
        this.trasfondo = Objects.requireNonNull(trasfondo);
    }
}
