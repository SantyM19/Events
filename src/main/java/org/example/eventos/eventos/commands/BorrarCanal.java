package org.example.eventos.eventos.commands;

import co.com.sofka.domain.generic.Command;
import org.example.eventos.eventos.value.CanalComunicacion;
import org.example.eventos.eventos.value.ReglamentosId;

public class BorrarCanal implements Command {
    private final ReglamentosId entityId;
    private final CanalComunicacion canal;

    public BorrarCanal(ReglamentosId entityId, CanalComunicacion canal) {
        this.entityId = entityId;
        this.canal = canal;
    }

    public ReglamentosId getEntityId() {
        return entityId;
    }

    public CanalComunicacion getCanal() {
        return canal;
    }
}
