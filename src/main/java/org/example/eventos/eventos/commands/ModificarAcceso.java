package org.example.eventos.eventos.commands;

import co.com.sofka.domain.generic.Command;
import org.example.eventos.eventos.value.Acceso;
import org.example.eventos.eventos.value.EventosId;
import org.example.eventos.eventos.value.ReglamentosId;

public class ModificarAcceso implements Command {
    private final EventosId eventoId;
    private final ReglamentosId entityId;
    private final Acceso acceso;

    public ModificarAcceso(EventosId eventoId, ReglamentosId entityId, Acceso acceso) {
        this.eventoId = eventoId;
        this.entityId = entityId;
        this.acceso = acceso;
    }

    public EventosId getEventoId() {
        return eventoId;
    }

    public ReglamentosId getEntityId() {
        return entityId;
    }

    public Acceso getAcceso() {
        return acceso;
    }
}
