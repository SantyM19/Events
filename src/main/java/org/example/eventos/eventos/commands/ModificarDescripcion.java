package org.example.eventos.eventos.commands;

import co.com.sofka.domain.generic.Command;
import org.example.eventos.eventos.value.DescripcionId;
import org.example.eventos.eventos.value.EventosId;
import org.example.eventos.eventos.value.Trasfondo;
import org.example.eventos.generics.value.Nombre;

public class ModificarDescripcion implements Command {
    private final EventosId eventoId;
    private final DescripcionId entityId;
    private final Nombre nombreEvento;
    private final Trasfondo trasfondo;

    public ModificarDescripcion(EventosId eventoId, DescripcionId entityId, Nombre nombreEvento, Trasfondo trasfondo) {
        this.eventoId = eventoId;
        this.entityId = entityId;
        this.nombreEvento = nombreEvento;
        this.trasfondo = trasfondo;
    }

    public EventosId getEventoId() {
        return eventoId;
    }

    public DescripcionId getEntityId() {
        return entityId;
    }

    public Nombre getNombreEvento() {
        return nombreEvento;
    }

    public Trasfondo getTrasfondo() {
        return trasfondo;
    }
}
