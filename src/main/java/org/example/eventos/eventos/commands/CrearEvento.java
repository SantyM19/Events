package org.example.eventos.eventos.commands;

import co.com.sofka.domain.generic.Command;
import org.example.eventos.eventos.Cronograma;
import org.example.eventos.eventos.Descripcion;
import org.example.eventos.eventos.Reglamento;
import org.example.eventos.eventos.value.EventosId;
import org.example.eventos.generics.Usuario;

import java.util.Objects;

public class CrearEvento implements Command {
    private final EventosId eventoId;
    private final Reglamento reglas;
    private final Descripcion descripcion;
    private final Cronograma cronograma;
    private final Usuario postor;

    public CrearEvento(EventosId eventoId, Reglamento reglas, Descripcion descripcion, Cronograma cronograma, Usuario postor) {
        this.eventoId = eventoId;
        this.reglas = reglas;
        this.descripcion = descripcion;
        this.cronograma = cronograma;
        this.postor = postor;
    }

    public EventosId getEventoId() {
        return eventoId;
    }

    public Reglamento getReglas() {
        return reglas;
    }

    public Descripcion getDescripcion() {
        return descripcion;
    }

    public Cronograma getCronograma() {
        return cronograma;
    }

    public Usuario getPostor() {
        return postor;
    }
}
