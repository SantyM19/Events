package org.example.eventos.eventos.commands;

import co.com.sofka.domain.generic.Command;
import org.example.eventos.eventos.value.CronogramaId;
import org.example.eventos.eventos.value.EventosId;
import org.example.eventos.eventos.value.FinEvento;

public class ModificarFechaFinEvento implements Command {
    private final EventosId eventoId;
    private final CronogramaId entityId;
    private final FinEvento finEvento;

    public ModificarFechaFinEvento(EventosId eventoId, CronogramaId entityId, FinEvento finEvento) {
        this.eventoId = eventoId;
        this.entityId = entityId;
        this.finEvento = finEvento;
    }

    public EventosId getEventoId() {
        return eventoId;
    }

    public CronogramaId getEntityId() {
        return entityId;
    }

    public FinEvento getFinEvento() {
        return finEvento;
    }
}
