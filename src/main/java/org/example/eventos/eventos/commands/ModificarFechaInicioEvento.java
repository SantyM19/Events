package org.example.eventos.eventos.commands;

import co.com.sofka.domain.generic.Command;
import org.example.eventos.eventos.value.CronogramaId;
import org.example.eventos.eventos.value.EventosId;
import org.example.eventos.eventos.value.InicioEvento;

public class ModificarFechaInicioEvento implements Command {
    private final EventosId eventoId;
    private final CronogramaId entityId;
    private final InicioEvento inicioEvento;

    public ModificarFechaInicioEvento(EventosId eventoId, CronogramaId entityId, InicioEvento inicioEvento) {
        this.eventoId = eventoId;
        this.entityId = entityId;
        this.inicioEvento = inicioEvento;
    }

    public EventosId getEventoId() {
        return eventoId;
    }

    public CronogramaId getEntityId() {
        return entityId;
    }

    public InicioEvento getInicioEvento() {
        return inicioEvento;
    }
}
