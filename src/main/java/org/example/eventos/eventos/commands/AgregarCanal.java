package org.example.eventos.eventos.commands;

import co.com.sofka.domain.generic.Command;
import org.example.eventos.eventos.value.CanalComunicacion;
import org.example.eventos.eventos.value.ReglamentosId;

public class AgregarCanal implements Command {
    private final ReglamentosId entityId;
    private final CanalComunicacion canal;

    public AgregarCanal(ReglamentosId entityId, CanalComunicacion canal) {
        this.entityId = entityId;
        this.canal = canal;
    }

    public ReglamentosId getEntityId() {
        return entityId;
    }

    public CanalComunicacion getCanal() {
        return canal;
    }
}
