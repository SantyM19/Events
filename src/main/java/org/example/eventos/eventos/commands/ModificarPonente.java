package org.example.eventos.eventos.commands;

import co.com.sofka.domain.generic.Command;
import org.example.eventos.eventos.value.EventosId;
import org.example.eventos.generics.value.Cuenta;
import org.example.eventos.generics.value.Edad;
import org.example.eventos.generics.value.Nickname;
import org.example.eventos.generics.value.UsuarioId;
import org.example.eventos.generics.value.Nombre;

public class ModificarPonente implements Command {
    private final EventosId eventoId;
    private final UsuarioId entityId;
    private final Nombre nombre;
    private final Cuenta cuenta;
    private final Nickname nickname;
    private final Edad edad;

    public ModificarPonente(EventosId eventoId, UsuarioId entityId, Nombre nombre, Cuenta cuenta, Nickname nickname, Edad edad) {
        this.eventoId = eventoId;
        this.entityId = entityId;
        this.nombre = nombre;
        this.cuenta = cuenta;
        this.nickname = nickname;
        this.edad = edad;
    }

    public EventosId getEventoId() {
        return eventoId;
    }

    public UsuarioId getEntityId() {
        return entityId;
    }

    public Nombre getNombre() {
        return nombre;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public Nickname getNickname() {
        return nickname;
    }

    public Edad getEdad() {
        return edad;
    }
}
