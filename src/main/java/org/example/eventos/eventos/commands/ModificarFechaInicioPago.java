package org.example.eventos.eventos.commands;

import co.com.sofka.domain.generic.Command;
import org.example.eventos.eventos.value.CronogramaId;
import org.example.eventos.eventos.value.EventosId;
import org.example.eventos.eventos.value.InicioPagos;

public class ModificarFechaInicioPago implements Command {
    private final EventosId eventoId;
    private final CronogramaId entityId;
    private final InicioPagos inicioPagos;

    public ModificarFechaInicioPago(EventosId eventoId, CronogramaId entityId, InicioPagos inicioPagos) {
        this.eventoId = eventoId;
        this.entityId = entityId;
        this.inicioPagos = inicioPagos;
    }

    public EventosId getEventoId() {
        return eventoId;
    }

    public CronogramaId getEntityId() {
        return entityId;
    }

    public InicioPagos getInicioPagos() {
        return inicioPagos;
    }
}
