package org.example.eventos.eventos;

import co.com.sofka.domain.generic.Entity;
import org.example.eventos.eventos.value.CronogramaId;
import org.example.eventos.eventos.value.FinEvento;
import org.example.eventos.eventos.value.InicioEvento;
import org.example.eventos.eventos.value.InicioPagos;

import java.util.Objects;

public class Cronograma extends Entity<CronogramaId> {
    private InicioPagos inicioPagos;
    private InicioEvento inicioEvento;
    private FinEvento finEvento;

    public Cronograma(CronogramaId entityId, InicioPagos inicioPagos, InicioEvento inicioEvento, FinEvento finEvento) {
        super(entityId);
        this.inicioPagos = inicioPagos;
        this.inicioEvento = inicioEvento;
        this.finEvento = finEvento;
    }

    public InicioPagos inicioPagos() {
        return inicioPagos;
    }

    public InicioEvento inicioEvento() {
        return inicioEvento;
    }

    public FinEvento finEvento() {
        return finEvento;
    }

    public void modificarInicioPagos(InicioPagos inicioPagos){
        this.inicioPagos = Objects.requireNonNull(inicioPagos);
    }
    public void modificarInicioEvento(InicioEvento inicioEvento){
        this.inicioEvento = Objects.requireNonNull(inicioEvento);
    }
    public void modificarFinEvento(FinEvento finEvento){
        this.finEvento = Objects.requireNonNull(finEvento);
    }
}
