package org.example.eventos.eventos.events;

import co.com.sofka.domain.generic.DomainEvent;
import org.example.eventos.eventos.value.CanalComunicacion;
import org.example.eventos.eventos.value.ReglamentosId;

public class CanalBorrado extends DomainEvent {
    private ReglamentosId entityId;
    private CanalComunicacion canal;

    public CanalBorrado(ReglamentosId entityId, CanalComunicacion canal) {
        super("example.eventos.canalborrado");
        this.canal = canal;
        this.entityId = entityId;
    }

    public ReglamentosId entityId() {
        return entityId;
    }

    public CanalComunicacion canal() {
        return canal;
    }
}
