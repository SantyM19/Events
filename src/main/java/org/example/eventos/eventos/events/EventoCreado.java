package org.example.eventos.eventos.events;

import co.com.sofka.domain.generic.DomainEvent;
import org.example.eventos.eventos.Descripcion;
import org.example.eventos.eventos.Cronograma;
import org.example.eventos.eventos.Reglamento;
import org.example.eventos.generics.Usuario;

public class EventoCreado extends DomainEvent {
    private final Reglamento reglas;
    private final Descripcion descripcion;
    private final Cronograma cronograma;
    private final Usuario postor;

    public EventoCreado(Reglamento reglas, Descripcion descripcion, Cronograma cronograma, Usuario postor) {
        super("example.eventos.eventocreado");
        this.reglas = reglas;
        this.descripcion = descripcion;
        this.cronograma = cronograma;
        this.postor = postor;
    }

    public Reglamento getReglas() {
        return reglas;
    }

    public Descripcion getDescripcion() {
        return descripcion;
    }

    public Cronograma getCronograma() {
        return cronograma;
    }

    public Usuario getPostor() {
        return postor;
    }
}
