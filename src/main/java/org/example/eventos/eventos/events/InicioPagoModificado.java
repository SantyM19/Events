package org.example.eventos.eventos.events;

import co.com.sofka.domain.generic.DomainEvent;
import org.example.eventos.eventos.value.CronogramaId;
import org.example.eventos.eventos.value.InicioPagos;

public class InicioPagoModificado extends DomainEvent {
    private InicioPagos inicioPagos;
    private CronogramaId entityId;

    public InicioPagoModificado(CronogramaId entityId, InicioPagos inicioPagos) {
        super("example.administrador.iniciopagomodificado");
        this.inicioPagos = inicioPagos;
        this.entityId = entityId;
    }

    public InicioPagos inicioPagos() {
        return inicioPagos;
    }

    public CronogramaId entityId() {
        return entityId;
    }
}
