package org.example.eventos.eventos.events;

import co.com.sofka.domain.generic.DomainEvent;
import org.example.eventos.eventos.value.Acceso;
import org.example.eventos.eventos.value.ReglamentosId;

public class AccesoModificado extends DomainEvent {
    private ReglamentosId entityId;
    private Acceso acceso;

    public AccesoModificado(ReglamentosId entityId, Acceso acceso) {
        super("example.eventos.accesomodificado");
        this.acceso = acceso;
        this.entityId = entityId;
    }

    public ReglamentosId entityId() {
        return entityId;
    }

    public Acceso acceso() {
        return acceso;
    }
}
