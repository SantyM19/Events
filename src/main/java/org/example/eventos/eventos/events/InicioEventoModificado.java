package org.example.eventos.eventos.events;

import co.com.sofka.domain.generic.DomainEvent;
import org.example.eventos.eventos.value.CronogramaId;
import org.example.eventos.eventos.value.InicioEvento;

public class InicioEventoModificado extends DomainEvent {
    private CronogramaId entityId;
    private InicioEvento inicioEvento;

    public InicioEventoModificado(CronogramaId entityId, InicioEvento inicioEvento) {
        super("example.administrador.inicioeventomodificado");
        this.entityId = entityId;
        this.inicioEvento = inicioEvento;
    }

    public CronogramaId entityId() {
        return entityId;
    }

    public InicioEvento inicioEvento() {
        return inicioEvento;
    }
}
