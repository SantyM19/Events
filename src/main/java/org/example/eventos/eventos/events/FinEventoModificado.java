package org.example.eventos.eventos.events;

import co.com.sofka.domain.generic.DomainEvent;
import org.example.eventos.eventos.value.CronogramaId;
import org.example.eventos.eventos.value.FinEvento;

public class FinEventoModificado extends DomainEvent {
    private CronogramaId entityId;
    private FinEvento finEvento;
    public FinEventoModificado(CronogramaId entityId, FinEvento finEvento) {
        super("example.administrador.fineventomodificado");
        this.entityId = entityId;
        this.finEvento = finEvento;
    }

    public CronogramaId entityId() {
        return entityId;
    }

    public FinEvento finEvento() {
        return finEvento;
    }
}
