package org.example.eventos.eventos.events;

import co.com.sofka.domain.generic.DomainEvent;
import org.example.eventos.eventos.value.CanalComunicacion;
import org.example.eventos.eventos.value.ReglamentosId;

public class CanalModificado extends DomainEvent {
    private ReglamentosId entityId;
    private CanalComunicacion canal;
    private CanalComunicacion newCanal;

    public CanalModificado(ReglamentosId entityId, CanalComunicacion canal, CanalComunicacion newCanal) {
        super("example.eventos.canalmodificado");
        this.entityId = entityId;
        this.canal = canal;
        this.newCanal = newCanal;
    }

    public ReglamentosId entityId() {
        return entityId;
    }

    public CanalComunicacion canal() {
        return canal;
    }

    public CanalComunicacion newCanal() {
        return newCanal;
    }
}
