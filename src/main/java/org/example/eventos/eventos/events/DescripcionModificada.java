package org.example.eventos.eventos.events;

import co.com.sofka.domain.generic.DomainEvent;
import org.example.eventos.eventos.value.DescripcionId;
import org.example.eventos.eventos.value.Trasfondo;
import org.example.eventos.generics.value.Nombre;

public class DescripcionModificada extends DomainEvent {
    private DescripcionId entityId;
    private Nombre nombreEvento;
    private Trasfondo trasfondo;

    public DescripcionModificada(DescripcionId entityId, Nombre nombreEvento, Trasfondo trasfondo) {
        super("example.administrador.descripcionmodificada");
        this.entityId = entityId;
        this.nombreEvento = nombreEvento;
        this.trasfondo = trasfondo;
    }

    public DescripcionId entityId() {
        return entityId;
    }

    public Nombre nombreEvento() {
        return nombreEvento;
    }

    public Trasfondo trasfondo() {
        return trasfondo;
    }
}
