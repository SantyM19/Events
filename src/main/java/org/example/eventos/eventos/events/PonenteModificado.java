package org.example.eventos.eventos.events;

import co.com.sofka.domain.generic.DomainEvent;
import org.example.eventos.generics.value.Cuenta;
import org.example.eventos.generics.value.Edad;
import org.example.eventos.generics.value.Nickname;
import org.example.eventos.generics.value.UsuarioId;
import org.example.eventos.generics.value.Nombre;

public class PonenteModificado extends DomainEvent {
    private UsuarioId entityId;
    private Nombre nombre;
    private Cuenta cuenta;
    private Nickname nickname;
    private Edad edad;

    public PonenteModificado(UsuarioId entityId, Nombre nombre, Cuenta cuenta, Nickname nickname, Edad edad) {
        super("example.eventos.ponentemodificado");
        this.entityId = entityId;
        this.nombre = nombre;
        this.cuenta = cuenta;
        this.nickname = nickname;
        this.edad = edad;
    }

    public UsuarioId entityId() {
        return entityId;
    }

    public Nombre nombre() {
        return nombre;
    }

    public Cuenta cuenta() {
        return cuenta;
    }

    public Nickname nickname() {
        return nickname;
    }

    public Edad edad() {
        return edad;
    }
}
