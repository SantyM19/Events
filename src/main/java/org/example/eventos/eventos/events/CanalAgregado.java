package org.example.eventos.eventos.events;

import co.com.sofka.domain.generic.DomainEvent;
import org.example.eventos.eventos.value.CanalComunicacion;
import org.example.eventos.eventos.value.ReglamentosId;

public class CanalAgregado extends DomainEvent {
    private ReglamentosId entityId;
    private CanalComunicacion canal;

    public CanalAgregado(ReglamentosId entityId, CanalComunicacion canal) {
        super("example.eventos.canalagregado");
        this.canal = canal;
        this.entityId = entityId;
    }

    public ReglamentosId entityId() {
        return entityId;
    }

    public CanalComunicacion canal() {
        return canal;
    }
}
