package org.example.usecase.notificar;

import co.com.sofka.business.generic.UseCase;
import co.com.sofka.business.support.ResponseEvents;
import co.com.sofka.business.support.TriggeredEvent;
import org.example.eventos.eventos.events.InicioEventoModificado;

public class NotificarCambioFechaInicioEventoUseCase extends UseCase<TriggeredEvent<InicioEventoModificado>, ResponseEvents> {
    @Override
    public void executeUseCase(TriggeredEvent<InicioEventoModificado> inicioEventoModificadoTriggeredEvent) {
        var event = inicioEventoModificadoTriggeredEvent.getDomainEvent();
        var comentarioService = getService(NotificarCambioFechaInicioEvento.class).orElseThrow();
        var senderEmailService = getService(SenderEmailService.class).orElseThrow();

        var email = comentarioService.getCorreoPorIdUsuario(event.entityId());
        senderEmailService.sendEmail(email, "Cambio de Fecha para los Pagos");
    }
}
