package org.example.usecase.notificar;

import org.example.eventos.generics.value.Cuenta;

public interface SenderEmailService {
    void sendEmail(Cuenta cuenta, String valoracion);
}
