package org.example.usecase.notificar;

import org.example.eventos.eventos.value.CronogramaId;
import org.example.eventos.generics.value.Cuenta;
import org.example.eventos.generics.value.UsuarioId;

public interface NotificarCambioFechaInicioPagos {
    Cuenta getCorreoPorIdUsuario(CronogramaId idUsuario);
}
