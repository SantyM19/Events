package org.example.usecase.notificar;

import co.com.sofka.business.generic.UseCase;
import co.com.sofka.business.support.ResponseEvents;
import co.com.sofka.business.support.TriggeredEvent;
import org.example.eventos.eventos.commands.ModificarFechaInicioPago;
import org.example.eventos.eventos.events.InicioPagoModificado;

public class NotificarCambioFechaInicioPagoUseCase extends UseCase<TriggeredEvent<InicioPagoModificado>, ResponseEvents> {
    @Override
    public void executeUseCase(TriggeredEvent<InicioPagoModificado> inicioPagoModificadoTriggeredEvent) {
        var event = inicioPagoModificadoTriggeredEvent.getDomainEvent();
        var comentarioService = getService(NotificarCambioFechaInicioPagos.class).orElseThrow();
        var senderEmailService = getService(SenderEmailService.class).orElseThrow();

        var email = comentarioService.getCorreoPorIdUsuario(event.entityId());
        senderEmailService.sendEmail(email, "Cambio de Fecha para los Pagos");
    }
}
