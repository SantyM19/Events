package org.example.usecase.notificar;

import org.example.eventos.eventos.value.CronogramaId;
import org.example.eventos.generics.value.Cuenta;

public interface NotificarCambioFechaInicioEvento {
    Cuenta getCorreoPorIdUsuario(CronogramaId idUsuario);

}
