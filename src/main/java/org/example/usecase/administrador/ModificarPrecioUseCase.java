package org.example.usecase.administrador;

import co.com.sofka.business.generic.UseCase;
import co.com.sofka.business.support.RequestCommand;
import co.com.sofka.business.support.ResponseEvents;
import org.example.eventos.administrador.Administrador;
import org.example.eventos.administrador.commands.ModificarPrecio;


public class ModificarPrecioUseCase extends UseCase<RequestCommand<ModificarPrecio>, ResponseEvents> {
    @Override
    public void executeUseCase(RequestCommand<ModificarPrecio> modificarPrecioRequestCommand) {
        var command = modificarPrecioRequestCommand.getCommand();
        var event = Administrador.from(command.getAdministradorId() ,retrieveEvents(command.getEntityId().value()));
        //aqui: reglas de negocio ...
        event.modificarPrecio(command.getEntityId(), command.getPrecio());
        emit().onResponse(new ResponseEvents(event.getUncommittedChanges()));
    }
}
