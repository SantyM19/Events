package org.example.usecase.administrador;

import co.com.sofka.business.generic.UseCase;
import co.com.sofka.business.support.RequestCommand;
import co.com.sofka.business.support.ResponseEvents;
import org.example.eventos.administrador.Administrador;
import org.example.eventos.administrador.commands.ModificarPago;

public class ModificarPagoUseCase extends UseCase<RequestCommand<ModificarPago>, ResponseEvents> {
    @Override
    public void executeUseCase(RequestCommand<ModificarPago> modificarPagoRequestCommand) {
        var command = modificarPagoRequestCommand.getCommand();
        var event = Administrador.from(command.getAdministradorId() ,retrieveEvents(command.getEntityId().value()));
        //aqui: reglas de negocio ...
        event.modificarPago(command.getEntityId(), command.getPago());
        emit().onResponse(new ResponseEvents(event.getUncommittedChanges()));
    }
}
