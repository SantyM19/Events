package org.example.usecase.administrador;

import co.com.sofka.business.generic.UseCase;
import co.com.sofka.business.support.RequestCommand;
import co.com.sofka.business.support.ResponseEvents;
import org.example.eventos.administrador.Administrador;
import org.example.eventos.administrador.commands.ModificarMedioVirtual;

public class ModificarMedioVirtualUseCase extends UseCase<RequestCommand<ModificarMedioVirtual>, ResponseEvents> {
    @Override
    public void executeUseCase(RequestCommand<ModificarMedioVirtual> modificarMedioVirtualRequestCommand) {
        var command = modificarMedioVirtualRequestCommand.getCommand();
        var event = Administrador.from(command.getAdministradorId() ,retrieveEvents(command.getEntityId().value()));
        //aqui: reglas de negocio ...
        event.modificarMedioVirtual(command.getEntityId(), command.getPlataforma(), command.getRegla());
        emit().onResponse(new ResponseEvents(event.getUncommittedChanges()));
    }
}
