package org.example.usecase.administrador;

import co.com.sofka.business.generic.UseCase;
import co.com.sofka.business.support.RequestCommand;
import co.com.sofka.business.support.ResponseEvents;
import org.example.eventos.administrador.Administrador;
import org.example.eventos.administrador.commands.CrearAdministrador;
import org.example.eventos.eventos.Evento;

public class CrearAdministradorUseCase extends UseCase<RequestCommand<CrearAdministrador>, ResponseEvents> {
    @Override
    public void executeUseCase(RequestCommand<CrearAdministrador> crearAdministradorRequestCommand) {
        var command = crearAdministradorRequestCommand.getCommand();
        var evento = new Administrador(command.getAdministradorId(),
                command.getMedioVirtual(),
                command.getParticipantes(),
                command.getRecaudos());
        //aqui: reglas de negocio ...
        emit().onResponse(new ResponseEvents(evento.getUncommittedChanges()));
    }
}
