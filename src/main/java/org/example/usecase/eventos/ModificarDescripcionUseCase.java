package org.example.usecase.eventos;

import co.com.sofka.business.generic.UseCase;
import co.com.sofka.business.support.RequestCommand;
import co.com.sofka.business.support.ResponseEvents;
import org.example.eventos.eventos.Evento;
import org.example.eventos.eventos.commands.ModificarDescripcion;

public class ModificarDescripcionUseCase extends UseCase<RequestCommand<ModificarDescripcion>, ResponseEvents> {
    @Override
    public void executeUseCase(RequestCommand<ModificarDescripcion> modificarDescripcionRequestCommand) {
        var command = modificarDescripcionRequestCommand.getCommand();
        var event = Evento.from(command.getEventoId() ,retrieveEvents(command.getEntityId().value()));
        //aqui: reglas de negocio ...
        event.modificarDescripcion(command.getEntityId(), command.getNombreEvento(),command.getTrasfondo());
        emit().onResponse(new ResponseEvents(event.getUncommittedChanges()));
    }
}
