package org.example.usecase.eventos;

import co.com.sofka.business.generic.UseCase;
import co.com.sofka.business.support.RequestCommand;
import co.com.sofka.business.support.ResponseEvents;
import org.example.eventos.eventos.Evento;
import org.example.eventos.eventos.commands.CrearEvento;

public class CrearEventoUseCase extends UseCase<RequestCommand<CrearEvento>, ResponseEvents> {

    @Override
    public void executeUseCase(RequestCommand<CrearEvento> crearEventoRequestCommand) {
        var command = crearEventoRequestCommand.getCommand();
        var evento = new Evento(command.getEventoId(), command.getReglas(), command.getDescripcion(), command.getCronograma(), command.getPostor());
        //aqui: reglas de negocio ...
        emit().onResponse(new ResponseEvents(evento.getUncommittedChanges()));
    }
}
