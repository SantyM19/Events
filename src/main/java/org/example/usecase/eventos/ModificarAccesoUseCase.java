package org.example.usecase.eventos;

import co.com.sofka.business.generic.UseCase;
import co.com.sofka.business.support.RequestCommand;
import co.com.sofka.business.support.ResponseEvents;
import org.example.eventos.eventos.Evento;
import org.example.eventos.eventos.commands.ModificarAcceso;

public class ModificarAccesoUseCase extends UseCase<RequestCommand<ModificarAcceso>, ResponseEvents> {

    @Override
    public void executeUseCase(RequestCommand<ModificarAcceso> modificarAccesoRequestCommand) {
        var command = modificarAccesoRequestCommand.getCommand();
        var event = Evento.from(command.getEventoId() ,retrieveEvents(command.getEntityId().value()));
        //aqui: reglas de negocio ...
        event.modificarAcceso(command.getEntityId(), command.getAcceso());
        emit().onResponse(new ResponseEvents(event.getUncommittedChanges()));
    }
}
