package org.example.usecase.eventos;

import co.com.sofka.business.generic.UseCase;
import co.com.sofka.business.support.RequestCommand;
import co.com.sofka.business.support.ResponseEvents;
import org.example.eventos.eventos.Evento;
import org.example.eventos.eventos.commands.ModificarFechaInicioEvento;

public class ModificarFechaInicioEventoUseCase extends UseCase<RequestCommand<ModificarFechaInicioEvento>, ResponseEvents> {

    @Override
    public void executeUseCase(RequestCommand<ModificarFechaInicioEvento> modificarFechaInicioEventoRequestCommand) {
        var command = modificarFechaInicioEventoRequestCommand.getCommand();
        var event = Evento.from(command.getEventoId() ,retrieveEvents(command.getEntityId().value()));
        //aqui: reglas de negocio ...
        event.modificarFechaInicioEvento(command.getEntityId(), command.getInicioEvento());
        emit().onResponse(new ResponseEvents(event.getUncommittedChanges()));
    }
}
