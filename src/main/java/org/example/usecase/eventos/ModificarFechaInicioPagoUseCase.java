package org.example.usecase.eventos;

import co.com.sofka.business.generic.UseCase;
import co.com.sofka.business.support.RequestCommand;
import co.com.sofka.business.support.ResponseEvents;
import org.example.eventos.eventos.Evento;
import org.example.eventos.eventos.commands.ModificarFechaInicioPago;

public class ModificarFechaInicioPagoUseCase extends UseCase<RequestCommand<ModificarFechaInicioPago>, ResponseEvents> {
    @Override
    public void executeUseCase(RequestCommand<ModificarFechaInicioPago> modificarFechaInicioPagoRequestCommand) {
        var command = modificarFechaInicioPagoRequestCommand.getCommand();
        var event = Evento.from(command.getEventoId() ,retrieveEvents(command.getEntityId().value()));
        //aqui: reglas de negocio ...
        event.modificarFechaInicioPago(command.getEntityId(), command.getInicioPagos());
        emit().onResponse(new ResponseEvents(event.getUncommittedChanges()));
    }
}
