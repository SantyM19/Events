package org.example.usecase.eventos;

import co.com.sofka.business.generic.UseCase;
import co.com.sofka.business.support.RequestCommand;
import co.com.sofka.business.support.ResponseEvents;
import org.example.eventos.eventos.Evento;
import org.example.eventos.eventos.commands.ModificarPonente;

public class ModificarPonenteUseCase extends UseCase<RequestCommand<ModificarPonente>, ResponseEvents> {
    @Override
    public void executeUseCase(RequestCommand<ModificarPonente> modificarPonenteRequestCommand) {
        var command = modificarPonenteRequestCommand.getCommand();
        var event = Evento.from(command.getEventoId() ,retrieveEvents(command.getEntityId().value()));
        //aqui: reglas de negocio ...
        event.modificarPonente(command.getEntityId(), command.getNombre(), command.getCuenta(), command.getNickname(), command.getEdad());
        emit().onResponse(new ResponseEvents(event.getUncommittedChanges()));
    }
}
