package org.example.usecase.eventos;

import co.com.sofka.business.generic.UseCase;
import co.com.sofka.business.support.RequestCommand;
import co.com.sofka.business.support.ResponseEvents;
import org.example.eventos.eventos.Evento;
import org.example.eventos.eventos.commands.ModificarFechaFinEvento;

public class ModificarFechaFinEventoUseCase extends UseCase<RequestCommand<ModificarFechaFinEvento>, ResponseEvents>
{

    @Override
    public void executeUseCase(RequestCommand<ModificarFechaFinEvento> modificarFechaFinEventoRequestCommand) {
        var command = modificarFechaFinEventoRequestCommand.getCommand();
        var event = Evento.from(command.getEventoId() ,retrieveEvents(command.getEntityId().value()));
        //aqui: reglas de negocio ...
        event.modificarFechaFinEvento(command.getEntityId(), command.getFinEvento());
        emit().onResponse(new ResponseEvents(event.getUncommittedChanges()));
    }
}
